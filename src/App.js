import logo from './logo.svg';
import './App.css';
import MainHeader from './components/frame/MainHeader';
import SideBar from './components/frame/SideBar';
import AllPlans from './components/main_elements/AllPlans';
import OfferDetails from './components/main_elements/OfferDetails';

function App() {
  return (
    <div className="App">
      <MainHeader/>
      <SideBar/>
      <br/>
      <AllPlans/>
      <br/>
      <OfferDetails/>
    </div>
  );
}

export default App;
