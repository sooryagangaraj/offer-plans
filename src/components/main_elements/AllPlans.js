import { Component } from "react"
import PlanCard from "./PlanCard";

const axios = require('axios');

class AllPlans extends Component {
    constructor(props) {
        super(props)

        this.state = {
          plans: [],
        }
        
        var self = this;

        // Fetch all plans
        axios.get('https://my-json-server.typicode.com/TypesetIO/mock/plans')
        .then(function (response) {
          self.setState({plans:response.data})

        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .then(function () {
          // always executed
        });
    }

    render() {
      
        return (<div className="container offers">
            <div className='row'>
              {/* List all plans From API */}
              { this.state.plans.map (plan => {
                return <PlanCard id={plan.id} title={plan.title} price={plan.price}/>  
                                
              })}
            </div>
            

      </div>)
    }
}


export default AllPlans