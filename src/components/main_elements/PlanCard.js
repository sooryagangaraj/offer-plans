import { Component } from "react"
import "./PlanCard.css"

class PlanCard extends Component {
    constructor(props) {
        super(props)
    }


    isSelectedClassNames = ()=> {
        var classNames= "card plan-card"
         var id = window.location.search.split('plan_id=')[1] ? window.location.search.split('plan_id=')[1] : '-';
         if (id != '-' && id == this.props.id) {
            classNames += ' selected'
         }
        return classNames
    }

    render() {
        return (
        <div className='col-4'>
            <a href={`/?plan_id=${this.props.id}`} className={this.isSelectedClassNames()}>
                <div className='card-title'>{this.props.title}</div>
                <div className='card-title'>{this.props.price}</div>
            </a>
        </div>
        )
    }
}


export default PlanCard