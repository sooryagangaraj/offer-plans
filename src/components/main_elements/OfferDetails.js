import { Component } from "react"

const axios = require('axios');

class OfferDetails extends Component {
    constructor(props) {
        super(props)

        this.state = {
            offers: [],
            id: window.location.search.split('plan_id=')[1] ? window.location.search.split('plan_id=')[1] : '-'
          }
          
          var self = this;
  
          // Fetch all offers
          axios.get('https://my-json-server.typicode.com/TypesetIO/mock/offers')
          .then(function (response) {
            self.setState({offers:response.data})
            
          })
          .catch(function (error) {
            // handle error
            console.log(error);
          })
          .then(function () {
            // always executed
          });
    }



    render() {
        return (
        <div className='container card'>
            {/* List all offers From API */}
            { this.state.offers.map (offer => {
                if (this.state.id == offer.id)
                    return <div > {offer.body} </div>  
                
              })}
        </div>
        )
    }
}


export default OfferDetails